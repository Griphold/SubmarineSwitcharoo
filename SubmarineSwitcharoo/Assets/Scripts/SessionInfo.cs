﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class SessionInfo : Singleton<SessionInfo> {

    private Dictionary<PlayerControl.Player, int> playerPoints;

    public Text[] playerPointsTexts;
    //public Color[] playerColors;
    public GameObject[] playerScoreTextPrefabs;
    public Transform gameCanvas;

    private void Start()
    {
        playerPoints = new Dictionary<PlayerControl.Player, int>();
        playerPoints.Add(PlayerControl.Player.ONE, 0);
        playerPoints.Add(PlayerControl.Player.TWO, 0);
        playerPoints.Add(PlayerControl.Player.THREE, 0);

        /*for (int i = 0; i < playerColors.Length; i++)
        {
            if (null != playerPointsTexts[i])
                playerPointsTexts[i].color = playerColors[i];
        }*/
    } 

    public int GetScore(PlayerControl.Player player)
    {
        return playerPoints[player];
    }

    public int GetHighestScore()
    {
        int max = -1;

        foreach (var p in (PlayerControl.Player[])Enum.GetValues(typeof(PlayerControl.Player)))
        {
            max = Mathf.Max(max, playerPoints[p]);
        }

        return max;
    }

    public void AddPoints(PlayerControl.Player player, int pointsToAdd, Vector3? pos = null)
    {
        playerPoints[player] += pointsToAdd;
        playerPoints[player] = Mathf.Max(0, playerPoints[player]);

        if (null != playerScoreTextPrefabs && null != pos)
        {
            Vector2 viewPos = Camera.main.WorldToViewportPoint(pos.Value);
            viewPos.x *= 1920f;
            viewPos.y *= 1080f;
            viewPos.y += 80f;
            Text scoreText = Instantiate(playerScoreTextPrefabs[(int)player], viewPos, Quaternion.identity, gameCanvas).GetComponent<Text>();
            scoreText.rectTransform.anchoredPosition = viewPos;
            scoreText.text = pointsToAdd.ToString();
        }

        if(null != playerPointsTexts[(int)player])
            playerPointsTexts[(int)player].text = playerPoints[player].ToString();
    }
}
