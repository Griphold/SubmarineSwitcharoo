﻿Shader "MBreezy/SDFText"
{
	Properties
	{
		[NoScaleOffset] _MainTex ("Texture", 2D) = "white" {}
		_Cutoff ("Cutoff", Float) = 0.5
		_Border ("Border", Float) = 0.2
		_BorderColor ("Border Color", Color) = (0,0,0,1)
		_CharacterSmoothing ("Character Smoothing", Float) = 0.1
		_BorderSmoothing ("Border Smoothing", Float) = 0.1
	}
	SubShader
	{
		Tags { "RenderType"="Overlay" "Queue"="Overlay" }

		Pass
		{
			Blend SrcAlpha OneMinusSrcAlpha
			ZTest Always

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
				float4 col : COLOR;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
				float4 col : COLOR;
			};

			sampler2D _MainTex;
			
			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = v.uv;
				o.col = v.col;
				return o;
			}
			
			float _Cutoff, _Border, _CharacterSmoothing, _BorderSmoothing;
			float4 _BorderColor;

			fixed4 frag (v2f i) : SV_Target
			{
				_BorderColor.a *= i.col.a;

				float a = 1.0 - tex2D(_MainTex, i.uv).a;
				
				if (_Cutoff - _CharacterSmoothing > a)
				{
					return lerp(_BorderColor, i.col, min(1, (_Cutoff - _CharacterSmoothing - a) / _CharacterSmoothing));
				}
				else
				{
					return lerp(_BorderColor, fixed4(_BorderColor.rgb, 0), (a - (_Cutoff + _Border)) / _BorderSmoothing);
				}
			}
			ENDCG
		}
	}
}
