﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CountUpPoints : MonoBehaviour {

    public PlayerControl.Player player;

    private Text number;

    public float targetYOffset = 400f;
    private float startY;

	void Start () {
        number = GetComponent<Text>();

        startY = number.rectTransform.anchoredPosition.y;

        StartCoroutine(CountUp());
	}

    private IEnumerator CountUp()
    {
        float targetY = startY + targetYOffset;

        int points = 0;

        do
        {
            number.text = points.ToString();
            Vector2 anchPos = number.rectTransform.anchoredPosition;
            anchPos.y = Mathf.SmoothStep(startY, targetY, ((float)points) / SessionInfo.instance.GetHighestScore());
            number.rectTransform.anchoredPosition = anchPos;
            yield return new WaitForSeconds(.05f);
            points += 32;
        } while (points < SessionInfo.instance.GetScore(player));
        number.text = Mathf.Min(SessionInfo.instance.GetScore(player), points).ToString();
    }
}
