﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(Camera))]
public class TrackMultiple : MonoBehaviour 
{
    //parameters
    public List<Transform> toTrack = new List<Transform>(1);
    public float moveSpeed = 5.0f;
    public float zoomSpeed = 5.0f;
    public float zoomOverheadFactor = 0.5f;
    public float mininumY;
    public float maximumY;

    //component references
    private Camera cam;

    //internal variables
    private float original_OrthoSize;

    //MonoBehaviour Events
	void Start () 
    {
        //get component references
        cam = GetComponent<Camera>();

        original_OrthoSize = cam.orthographicSize;
	}

	void LateUpdate () 
    {

        Vector2 centroid = Vector2.zero;//new target position = centroid of polygon formed by positions of all tracked objects

        foreach (Transform t in toTrack)
        {
            if (t == null) continue;

            centroid += (Vector2)t.position;
        }

        centroid /= toTrack.Count;//centroid = avg position

        //interpolate position
        transform.position = Vector3.Lerp(transform.position, new Vector3(centroid.x, centroid.y, transform.position.z), Time.smoothDeltaTime * moveSpeed);


        //calculate new target zoom (ortho size)
        float maxDist_x = 0;
        float maxDist_y = 0;

        //calculate maximum x and y distance from camera midpoint
        foreach (Transform t in toTrack)
        {
            if (t == null) continue;

            float dist_x = Mathf.Abs(t.position.x - transform.position.x);
            float dist_y = Mathf.Abs(t.position.y - transform.position.y);

            if (dist_x > maxDist_x) { maxDist_x = dist_x; }
            if (dist_y > maxDist_y) { maxDist_y = dist_y; }
        }

        
        //calculate orthosize (y-halfsize) from max x distance
        float orthoSize = maxDist_x / cam.aspect;

        if (orthoSize < maxDist_y) { orthoSize = maxDist_y; }//objects are further apart in y than they are in x
        if (orthoSize < original_OrthoSize) { orthoSize = original_OrthoSize; }//dont zoom in further than original zoom


        //cam.orthographicSize = Mathf.Lerp(cam.orthographicSize, orthoSize + zoomOverheadFactor * original_OrthoSize, zoomSpeed * Time.smoothDeltaTime);//interpolate zoom

        cam.transform.position = new Vector3(transform.position.x, Mathf.Clamp(transform.position.y, mininumY, maximumY), transform.position.z);
	}
}
