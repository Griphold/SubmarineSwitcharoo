﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectibleCloud : MonoBehaviour {

    public GameObject[] collectibles;

    public int collectibleCount = 40;

    public float spawnRadius = 5f;

    [Range(0f, 1f)]
    public float firstPlayerPercentage = .5f;

    [Range(0f, 1f)]
    public float twoPlayerPercentage = .25f;

    public void Spawn(PlayerControl.Player? prominentPlayer = null)
    {
        List<PlayerControl.Player> allPlayers = new List<PlayerControl.Player>(
                    (PlayerControl.Player[])System.Enum.GetValues(typeof(PlayerControl.Player)));

        prominentPlayer = prominentPlayer ?? allPlayers[Random.Range(0, allPlayers.Count)];

        List<PlayerControl.Player> nonDominantPlayers = new List<PlayerControl.Player>((PlayerControl.Player[])System.Enum.GetValues(typeof(PlayerControl.Player)));
        nonDominantPlayers.Remove(prominentPlayer.Value);
        
        for  (int i = 0; i < collectibleCount; i++)
        {
            PlayerControl.Player mainPlayer;
            mainPlayer = prominentPlayer.Value;

            if (Random.value > firstPlayerPercentage)
                mainPlayer = nonDominantPlayers[Random.Range(0, nonDominantPlayers.Count)];
            else
                mainPlayer = prominentPlayer.Value;

            Collectible cunt = Instantiate(collectibles[Random.Range(0, collectibles.Length)],
                transform.position + (Vector3)Random.insideUnitCircle * spawnRadius,
                Quaternion.Euler(0f, 0f, Random.Range(0f, 360f)),
                transform).GetComponent<Collectible>();

            /*if (Random.value < twoPlayerPercentage)
            {
                allPlayers.Clear();
                allPlayers.AddRange((PlayerControl.Player[])System.Enum.GetValues(typeof(PlayerControl.Player)));

                allPlayers.Remove(mainPlayer);
                
                cunt.SetAffiliation(mainPlayer, allPlayers[Random.Range(0, allPlayers.Count)]);
            }
            else*/
            {
                cunt.SetAffiliation(mainPlayer);
            }
        }
    }

    private void OnDrawGizmos()
    {
        Color blue = Color.blue;
        blue.a = 0.3f;
        Gizmos.color = blue;

        Gizmos.DrawSphere(transform.position, spawnRadius);
    }
}
