﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mine : Obstacle 
{
    //point value
    [Range(0, 1)]
    public float HazardValue;

    //effects
    public GameObject ExplosionEffect;

    //component references
    [SerializeField]
    private CircleCollider2D trigger;
    [SerializeField]
    private PointEffector2D force;

    private SpriteRenderer render;

    //appearance stuff    
    public Color blinkColor;
    private Material mat;
    private Color originalColor;

    public float maxBlinkRate = 30.0f;
    public AnimationCurve blinkByDistance;
    private float blinkRate = 0.0f;

    private float t = 0.0f;

    void Start()
    {
        render = GetComponent<SpriteRenderer>();
        originalColor = render.color;
    }

    void Update()
    {
        //blink if blink rate high enough
        if (blinkRate > 0.01f)
        {
            t = t + blinkRate * Time.deltaTime;
            if (t > Mathf.PI * 2) { t = 0.0f; }

            render.color = Color.Lerp(originalColor, blinkColor, Mathf.Sin(t));
        }
        else//otherwise fade to original color
        {
            render.color = Color.Lerp(render.color, originalColor, Time.deltaTime * 2.5f);
        }

    }

    void OnTriggerStay2D(Collider2D other)
    {
        //if player in trigger, calculate blink rate by distance
        if (other.gameObject.layer == LayerMask.NameToLayer("Player"))
        {
            float distNormalized = Vector3.Distance(transform.position, other.transform.position) / (transform.localScale.x * trigger.radius);
            blinkRate = blinkByDistance.Evaluate(distNormalized) * maxBlinkRate;
        }
    }

    void OnCollisionEnter2D(Collision2D coll)
    {
        base.OnCollisionEnter2D(coll);

        if (coll.gameObject.layer == LayerMask.NameToLayer("Player"))
        {
            //remove points from every player
            SessionInfo s = SessionInfo.instance;
            s.AddPoints(PlayerControl.Player.ONE, Mathf.RoundToInt(-HazardValue * s.GetScore(PlayerControl.Player.ONE)));
            s.AddPoints(PlayerControl.Player.TWO, Mathf.RoundToInt(-HazardValue * s.GetScore(PlayerControl.Player.TWO)));
            s.AddPoints(PlayerControl.Player.THREE, Mathf.RoundToInt(-HazardValue * s.GetScore(PlayerControl.Player.THREE)));

            StartCoroutine(Explode());
        }
        else if (coll.gameObject.layer == LayerMask.NameToLayer("Projectile"))
        {
            StartCoroutine(Explode());
        }
    }

    private IEnumerator Explode()
    {
        //Instantiate explosion
        GameObject effect = Instantiate(ExplosionEffect, transform.position, Quaternion.identity);
        ParticleSystem pSys = effect.GetComponent<ParticleSystem>();

        render.enabled = false;

        force.enabled = true;
        yield return new WaitForSeconds(pSys.duration * 0.1f);
        force.enabled = false;

        Destroy(gameObject);
    }
}
