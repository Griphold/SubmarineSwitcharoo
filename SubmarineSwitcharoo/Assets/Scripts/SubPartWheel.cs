﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class SubPartWheel : MonoBehaviour {

    private SubmarinePart.Type partOnTop = SubmarinePart.Type.GUN;

    private readonly Dictionary<SubmarinePart.Type, float> subPartToAngle = new Dictionary<SubmarinePart.Type, float>
    {
        { SubmarinePart.Type.GUN, 0f },
        { SubmarinePart.Type.HOOK, -120f },
        { SubmarinePart.Type.PROPELLER, -240f }
    };

    public Image inner;

    public float spinDuration = 1f;

    private void Start()
    {
        PlayerControl.instance.OnCycle += CycleHandler;
    }

    private void OnDestroy()
    {
        //PlayerControl.instance.OnCycle -= CycleHandler;
    }

    private void CycleHandler()
    {
        StartCoroutine(Rotate());
    }

    private IEnumerator Rotate()
    {
        float startAngle = subPartToAngle[partOnTop];
        float targetAngle = startAngle - 120f;

        partOnTop = subPartToAngle.Keys.ToList()[(subPartToAngle.Keys.ToList().IndexOf(partOnTop) + 1) % 3];

        float t = 0f;

        do
        {
            inner.transform.rotation = Quaternion.Euler(0f, 0f,
                Mathf.SmoothStep(startAngle, targetAngle, Mathf.Min(1f, t / spinDuration)));
            t += Time.deltaTime;
            yield return null;
        } while (t < spinDuration);
    }

    private void Update()
    {
        // PlayerControl.instance.GetCycleProgress();
    }
}
