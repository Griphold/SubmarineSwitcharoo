﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IKillMySelfAfter : MonoBehaviour {

    public float lifeTime = 3f;

	void Start () {
        StartCoroutine(Die());
	}
	
    private IEnumerator Die()
    {
        yield return new WaitForSeconds(lifeTime);
        Destroy(gameObject);
    }
}
