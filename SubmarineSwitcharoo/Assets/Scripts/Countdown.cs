﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Countdown : MonoBehaviour {

    public Text countdownNumbers, cycleText;

    public float timePerLetter = .7f;

    private bool running = false;

	private void Update () {
        if (!running && PlayerControl.instance.GetTimeUntilNextCycle() <= 3f * .7f)
            StartCoroutine(CountdownCR());
	}

    private IEnumerator CountdownCR()
    {
        running = true;
        countdownNumbers.text = "3";
        yield return new WaitForSeconds(timePerLetter);
        countdownNumbers.text = "2";
        yield return new WaitForSeconds(timePerLetter);
        countdownNumbers.text = "1";
        yield return new WaitForSeconds(timePerLetter);
        countdownNumbers.text = "";
        cycleText.text = "CYCLE!";
        yield return new WaitForSeconds(2f);
        cycleText.text = "";
        running = false;
    }
}
