﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControl : Singleton<PlayerControl> {

    public enum Player {
        ONE, TWO, THREE
    }

    [System.Serializable]
    public class InputSettings
    {
        public string XAxisName;
        public string YAxisName;
        public string AlternativeXAxisName;
        public string AlternativeYAxisName;
        public KeyCode ActionName = KeyCode.Joystick1Button0;
        public KeyCode AlternativeActionName;
    }


    public Color[] playerColors;

    public float MaximumVelocity;

    public InputSettings player1, player2, player3;

    //Player1
    public SubmarinePart player1Part;
    public SubmarinePart player2Part;
    public SubmarinePart player3Part;

    public bool cycle;
    public float cycleTime;

    public delegate void CycleHandler();
    public event CycleHandler OnCycle;

    private float currentCycleTime;

    private Rigidbody2D SubmarineRigidbody;

    //save input axes


    // Use this for initialization
    void Start() {
        currentCycleTime = 0;

        player1Part.SetControllingPlayer(Player.ONE);
        player1Part.RotatePart(0, false);
        player2Part.SetControllingPlayer(Player.TWO);
        player2Part.RotatePart(240, false);
        player3Part.SetControllingPlayer(Player.THREE);
        player3Part.RotatePart(120, false);

        SubmarineRigidbody = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update() {
        ResolveInput(player1, player1Part);
        ResolveInput(player2, player2Part);
        ResolveInput(player3, player3Part);

        if (cycle) {
            currentCycleTime += Time.deltaTime;

            if (currentCycleTime > cycleTime)
            {
                Cycle();
            }
        }
    }

    void FixedUpdate()
    {
        Vector2 vel = SubmarineRigidbody.velocity;

        if (vel.SqrMagnitude() > MaximumVelocity * MaximumVelocity)
        {
            SubmarineRigidbody.velocity = vel.normalized * MaximumVelocity;
        }
    }

    public Player GetPlayer(SubmarinePart.Type part)
    {
        if (player1Part.PartType == part)
        {
            return PlayerControl.Player.ONE;
        }
        else if (player2Part.PartType == part)
        {
            return PlayerControl.Player.TWO;
        }
        else //if player3Part.PartType == part
        {
            return PlayerControl.Player.THREE;
        }
    }

    public SubmarinePart.Type GetPart(Player player)
    {
        if(player1Part.GetControllingPlayer() == player)
        {
            return player1Part.PartType;
        }
        else if(player2Part.GetControllingPlayer() == player)
        {
            return player2Part.PartType;
        }
        else //if(player3Part.GetControllingPlayer() == player)
        {
            return player3Part.PartType;
        }
    }

    public SubmarinePart GetPart(SubmarinePart.Type part)
    {
        if (player1Part.PartType == part)
        {
            return player1Part;
        }
        else if (player2Part.PartType == part)
        {
            return player2Part;
        }
        else //if(player3Part.GetControllingPlayer() == player)
        {
            return player3Part;
        }
    }


    //1 == cycle, 0.2 == 20% progress until cycle
    public float GetCycleProgress()
    {
        return  currentCycleTime / cycleTime;
    }

    public float GetTimeUntilNextCycle()
    {
        return cycleTime - currentCycleTime;
    }

    private Vector3[] activateGizmoPos = new Vector3[3];
    private bool[] activateGizmoButton = new bool[3];
    public void OnDrawGizmos()
    {
        if (activateGizmoPos == null)
            activateGizmoPos = new Vector3[3];

        if (SessionInfo.instance == null)
            return;

        Vector3 playerDir = activateGizmoPos[0];
        Gizmos.color = playerColors[0];
        
        if(activateGizmoButton[0])
            Gizmos.DrawSphere(transform.position + new Vector3(playerDir.x, playerDir.y, 0), 0.2f);
        Gizmos.DrawLine(transform.position, transform.position + new Vector3(playerDir.x, playerDir.y, 0));

        playerDir = activateGizmoPos[1];
        Gizmos.color = playerColors[1];

        if (activateGizmoButton[1])
            Gizmos.DrawSphere(transform.position + new Vector3(playerDir.x, playerDir.y, 0), 0.2f);
        Gizmos.DrawLine(transform.position, transform.position + new Vector3(playerDir.x, playerDir.y, 0));

        playerDir = activateGizmoPos[2];
        Gizmos.color = playerColors[2];

        if (activateGizmoButton[2])
            Gizmos.DrawSphere(transform.position + new Vector3(playerDir.x, playerDir.y, 0), 0.2f);
        Gizmos.DrawLine(transform.position, transform.position + new Vector3(playerDir.x, playerDir.y, 0));
    }


    private void ResolveInput(InputSettings input, SubmarinePart part)
    {
        //get player input
        float playerX = Input.GetAxis(input.XAxisName);
        float playerY = Input.GetAxis(input.YAxisName);
        float playerAltX = Input.GetAxis(input.AlternativeXAxisName);
        float playerAltY = Input.GetAxis(input.AlternativeYAxisName);

        //get player direction
        Vector2 playerDir = new Vector2(playerX, playerY);
        Vector2 altPlayerdir = new Vector2(playerAltX, playerAltY);

        //always take the bigger of the two inputs
        if(playerDir.sqrMagnitude < altPlayerdir.sqrMagnitude)
        {
            playerDir = altPlayerdir;
        }

        if (playerDir.magnitude > 1)
        {
            playerDir = playerDir.normalized;
        }

        if(input == player1)
        {
            activateGizmoPos[0] = playerDir;
            activateGizmoButton[0] = Input.GetKey(input.ActionName);
        } else if (input == player2)
        {
            activateGizmoPos[1] = playerDir;
            activateGizmoButton[1] = Input.GetKey(input.ActionName);
        } else //if (input == player3)
        {
            activateGizmoPos[2] = playerDir;
            activateGizmoButton[2] = Input.GetKey(input.ActionName);
        }
        

        float angleFromUp = Vector2.Angle(Vector2.up, playerDir) * Mathf.Sign(-playerDir.x);

        if (playerDir.magnitude > 0.5f)
        {
            part.RotatePart(angleFromUp);
        }

        //if either alt or normal key are activated, activate the part
        part.Activate(Input.GetKey(input.ActionName) || Input.GetKey(input.AlternativeActionName));

    }

    private void Cycle()
    {
        //set rotation to the rotation of the old part
        float player1Angle = player1Part.transform.eulerAngles.z;
        float player2Angle = player2Part.transform.eulerAngles.z;
        float player3Angle = player3Part.transform.eulerAngles.z;

        //change controlling player
        SubmarinePart p = player3Part;
        player3Part = player2Part;
        player2Part = player1Part;
        player1Part = p;

        player1Part.SetControllingPlayer(Player.ONE);
        player2Part.SetControllingPlayer(Player.TWO);
        player3Part.SetControllingPlayer(Player.THREE);

        player1Part.RotatePart(player1Angle, false);
        player2Part.RotatePart(player2Angle, false);
        player3Part.RotatePart(player3Angle, false);

        //call oncycle even
        if(OnCycle != null)
        {
            OnCycle();
        }

        currentCycleTime = 0;

        //Debug.Log("Cycle");
    }
}
