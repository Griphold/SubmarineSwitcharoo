﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimeLimit : MonoBehaviour {

    public int secondsPerRound = 120;

    public string endSceneName;

    private Text myText;

    private void Start()
    {
        myText = GetComponent<Text>();

        StartCoroutine(Countdown());
    }

    private IEnumerator Countdown()
    {
        int secondsLeft = secondsPerRound;

        do
        {
            int minutes = secondsLeft / 60;
            int seconds = secondsLeft % 60;

            myText.text = minutes + ":" + seconds.ToString("00");

            yield return new WaitForSeconds(1f);
            secondsLeft--;
        } while (secondsLeft > 0);

        PlayerControl.instance = null;
        UnityEngine.SceneManagement.SceneManager.LoadScene(endSceneName);
    }
}
