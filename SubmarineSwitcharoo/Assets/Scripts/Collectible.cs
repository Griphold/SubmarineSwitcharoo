﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collectible : MonoBehaviour {

    public int pointValue;

    private List<PlayerControl.Player> positivePlayers = new List<PlayerControl.Player>();

    private float spawnTime;

    float minDistanceToPlayerSpawn = 2f;

    private void Start()
    {
        spawnTime = Time.time;

        if ((PlayerControl.instance.transform.position - transform.position).magnitude < minDistanceToPlayerSpawn)
            Destroy(gameObject);
    }

    public void SetAffiliation(params PlayerControl.Player[] players)
    {
        positivePlayers.AddRange(players);
        GetComponent<SpriteRenderer>().color = PlayerControl.instance.playerColors[(int) players[0]];
    }

    public List<PlayerControl.Player> GetAffiliation()
    {
        return positivePlayers;
    }

    public void OnTriggerEnter2D(Collider2D collider)
    {
        //we are colliding with the player
        PlayerControl pc = PlayerControl.instance;
        PlayerControl.Player p;

        //if a projectile shoots this collectible, collect for the player who shot the projectile
        if (collider.CompareTag("Projectile"))
        {
            p = collider.gameObject.GetComponent<GunProjectile>().player;

            //add points only if i shoot my own collectibles
            if (p == positivePlayers[0])
            {
                SessionInfo.instance.AddPoints(positivePlayers[0], pointValue, transform.position);
            }

            Destroy(gameObject);
            Destroy(collider.gameObject);
            return;
            //Debug.Log(p + ":" + pointValue);
        }
        //if the player ship collider with this collectible, collect for the player controlling the propeller
        else if (collider.CompareTag("Player"))
        {
            p = collider.gameObject.GetComponent<PlayerControl>().GetPlayer(SubmarinePart.Type.PROPELLER);

            //add points
            SessionInfo.instance.AddPoints(positivePlayers[0], pointValue, transform.position);

            Destroy(gameObject);
            return;
            //Debug.Log(p + ":" + pointValue);
        }
        //if the player claw collides with this collectible
        else if (collider.CompareTag("Hook"))
        {
            Hook hook = (Hook) PlayerControl.instance.GetPart(SubmarinePart.Type.HOOK);
            hook.OnCollectibleGrab(this);
            return;
        }

        //destroy collectibles when colliding with other collectibles at spawn time
        if (LayerMask.NameToLayer("Collectibles") == collider.gameObject.layer && (Time.time - spawnTime) < 0.2f)
        {
            Destroy(gameObject);
            return;
        }
        else if ("Hazard" != collider.gameObject.tag && LayerMask.NameToLayer("Collectibles") != collider.gameObject.layer)
        {
            Destroy(gameObject);
            return;
        }
        else if (!collider.isTrigger)
        {
            Destroy(gameObject);
            return;
        }

    }
}
