﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
[RequireComponent(typeof(LineRenderer))]
public class Chain : MonoBehaviour 
{
    private LineRenderer lineRenderer;
    public Transform chainEnd;

	void Start () 
    {
		lineRenderer = GetComponent<LineRenderer>();
	}
	
	void LateUpdate () 
    {
        Vector3[] positions = { transform.position - Vector3.back, chainEnd.position - Vector3.back };
        lineRenderer.SetPositions(positions);
	}
}
