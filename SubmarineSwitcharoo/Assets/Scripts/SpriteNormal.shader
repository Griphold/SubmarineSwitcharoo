﻿// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

Shader "Unlit/SpriteNormal"
{
	Properties
	{
		[PerRendererData] _MainTex("Sprite Texture", 2D) = "white" {}
		_NormalMap("Normal Map", 2D) = "bump" {}
		_Color("Tint", Color) = (1,1,1,1)
			_Boost ("Flatness Normal", Float) = 1
		_Diffuse ("Diffuse stregth", Float) = 1
	}
	SubShader
	{
		Tags
		{
			"Queue" = "Transparent"
			"RenderType" = "Transparent"
		}

		Pass
		{
			Tags
			{
			"LightMode" = "ForwardBase"
			}

			Blend SrcAlpha OneMinusSrcAlpha
			Cull Off
			ZWrite Off

			CGPROGRAM
			#pragma multi_compile_fwdbase 
			#pragma vertex vert
			#pragma fragment frag

			#include "UnityCG.cginc"
			#include "UnityLightingCommon.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float4 pos : SV_POSITION;
				float2 uv : TEXCOORD1;
			};		
			
			v2f vert (appdata v)
			{
				v2f o;
				o.pos = UnityObjectToClipPos(v.vertex);
				o.uv = v.uv;

				return o;
			}
			
			sampler2D _MainTex, _NormalMap;
			float4 _Color;
			float _Boost, _Diffuse;

			fixed4 frag (v2f i) : SV_Target
			{
				float3 nor = half4(tex2D(_NormalMap, i.uv).rgb,0);
				nor.z = -1 * _Boost;

				nor = mul(normalize(nor), unity_WorldToObject);

				half3 diff = ShadeSH9(half4(nor, 1));
				half nl = max(0, dot(nor, _WorldSpaceLightPos0.xyz));
				diff += nl * _LightColor0 * _Diffuse;

				fixed4 col = tex2D(_MainTex, i.uv) * _Color;
				col.rgb *= diff.rgb;

				return col;
			}
			ENDCG
		}





			Pass
			{
				Tags
			{
				"LightMode" = "ForwardAdd"
			}

				Blend SrcAlpha One
				Cull Off
				ZWrite Off

				CGPROGRAM
#pragma multi_compile_fwdadd 
#pragma vertex vert
#pragma fragment frag

#include "UnityCG.cginc"
#include "UnityLightingCommon.cginc"

				struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float4 pos : SV_POSITION;
				float3 posWorld : TEXCOORD0;
				float2 uv : TEXCOORD1;
			};

			v2f vert(appdata v)
			{
				v2f o;
				o.pos = UnityObjectToClipPos(v.vertex);
				o.posWorld = mul(unity_ObjectToWorld, v.vertex);
				o.uv = v.uv;

				return o;
			}

			sampler2D _MainTex, _NormalMap;
			float4 _Color;
			float _Boost, _Diffuse;

			fixed4 frag(v2f i) : SV_Target
			{
				float3 nor = half4(tex2D(_NormalMap, i.uv).rgb,0);
				nor.z = -1 * _Boost;

				nor = mul(normalize(nor), unity_WorldToObject);

				half3 diff = half3(0,0,0);
				half nl = max(0, dot(nor, normalize(_WorldSpaceLightPos0.xyz - i.posWorld)));
				float dist = distance(_WorldSpaceLightPos0.xyz, i.posWorld);
				diff += nl * _LightColor0 * _Diffuse * 1 / dist;

				fixed col = tex2D(_MainTex, i.uv).a;

				return fixed4(diff.rgb, col);
			}
				ENDCG
			}

	}
}
