﻿using UnityEngine;
using System.Collections;

public class Parallax : MonoBehaviour 
{

    private static float scrollSpeed = 0.01f;


    public int depth = 1;
    private Camera cam;

    private Material mat;

    void Start()
    {       
        cam = Camera.main;
        mat = GetComponent<Renderer>().material;
    }


    void LateUpdate()
    {
        transform.position = new Vector3(cam.transform.position.x, transform.position.y, transform.position.z);
        mat.mainTextureOffset = new Vector2(-cam.transform.position.x * scrollSpeed * depth, mat.mainTextureOffset.y);
    }
}
