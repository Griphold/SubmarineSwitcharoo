﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScaleBarWithPoints : MonoBehaviour {

    private Image bar;

    public PlayerControl.Player player;

    public float targetHeightOffset = 390f;

	void Start () {
        bar = GetComponent<Image>();

        StartCoroutine(ScaleUp());
	}

    private IEnumerator ScaleUp()
    {
        const float minHeight = 50f;
        float targetHeight = minHeight + targetHeightOffset;

        int points = 0;
        do
        {
            Vector2 soice = bar.rectTransform.sizeDelta;
            soice.y = Mathf.SmoothStep(minHeight, targetHeight, (float)points / SessionInfo.instance.GetHighestScore());
            bar.rectTransform.sizeDelta = soice;
            yield return new WaitForSeconds(.05f);
            points += 32;
        } while (points < SessionInfo.instance.GetScore(player));

        points = Mathf.Min(SessionInfo.instance.GetScore(player));
        Vector2 size = bar.rectTransform.sizeDelta;
        size.y = Mathf.SmoothStep(minHeight, targetHeight, (float)points / SessionInfo.instance.GetHighestScore());
        bar.rectTransform.sizeDelta = size;

    }
}
