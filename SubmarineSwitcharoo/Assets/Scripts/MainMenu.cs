﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour 
{
    public Canvas canvas;
    public GameObject uiEventSys;
    public Transform background;
    public SpriteRenderer spriteRenderer;

    public float animationSpeed;
    public AnimationCurve backgroundScollAnimation;

    void Start()
    {
        if(SessionInfo.instance != null)
        { 
            Destroy(SessionInfo.instance.gameObject);
            SessionInfo.instance = null;
        }
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Return)){
            OnStartButton();
        }
        else if (Input.GetKeyDown(KeyCode.Escape))
        {
            OnEndButton();
        }
    }

    public void OnStartButton()
    {
        canvas.gameObject.SetActive(false);
        uiEventSys.SetActive(false);

        StartCoroutine(StartAnimation());
    }

    public void OnEndButton()
    {
        Application.Quit();
    }

    private IEnumerator StartAnimation()
    {
        Vector3 originalBGPos = background.position;

        for (float p = 0.0f; p < 1.0f; p += animationSpeed * Time.deltaTime)
        {
            background.position = originalBGPos + Vector3.up * backgroundScollAnimation.Evaluate(p) * background.localScale.z;
            spriteRenderer.color = new Color(spriteRenderer.color.r, spriteRenderer.color.g, spriteRenderer.color.b, p);
            yield return null;
        }

        Destroy(PlayerControl.instance.gameObject);
        PlayerControl.instance = null;

        SceneManager.LoadScene("testscene");
            
    }
}
