﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldSpawner : MonoBehaviour {

    public Vector2 sectorDimensions = Vector2.one * 10;

    private List<int> generatedSectors;

    private Dictionary<int, GameObject> generatedSectorParents;

    public int activeSectorMaxDistance = 4, deleteSectorsAtDistance = 6;

    [Header("Per sector items")]
    public GameObject[] obstacles;
    public int obstacleCount = 5;

    public GameObject[] clouds;
    public int cloudCount = 8;

    public GameObject background;
    
    private void Update()
    {
        int currentSector =
            Mathf.RoundToInt(PlayerControl.instance.transform.position.x / sectorDimensions.x);

        GenerateSurroundingSectors(currentSector);

        CleanupFarSectors(currentSector);
    }

    private void Start()
    {
        generatedSectors = new List<int>();
        generatedSectorParents = new Dictionary<int, GameObject>();
    }

    private void GenerateSurroundingSectors(int sectorNumber)
    {
        for (int sector = sectorNumber - activeSectorMaxDistance; sector <= sectorNumber + activeSectorMaxDistance; sector++)
        {
            if (!generatedSectors.Contains(sector))
            {
                GameObject sectorParent = new GameObject();
                sectorParent.name = "Sector " + sector;
                sectorParent.transform.parent = transform;

                generatedSectorParents.Add(sector, sectorParent);

                for (int i = 0; i < obstacleCount; i++)
                {
                    Instantiate(obstacles[Random.Range(0, obstacles.Length)],
                        RandomSectorPosition(sector),
                        Quaternion.Euler(0f, 0f, Random.Range(0f, 360f)),
                        sectorParent.transform);
                }

                for (int i = 0; i < cloudCount; i++)
                {
                    CollectibleCloud cc = Instantiate(clouds[Random.Range(0, clouds.Length)],
                        RandomSectorPosition(sector),
                        Quaternion.Euler(0f, 0f, Random.Range(0f, 360f)),
                        sectorParent.transform).GetComponent<CollectibleCloud>();

                    cc.Spawn();
                }

                //background
                if (background != null)
                {
                    GameObject bgLeft = Instantiate(background, sectorParent.transform.position, background.transform.rotation, sectorParent.transform);
                    GameObject bgRight = Instantiate(background, sectorParent.transform.position, background.transform.rotation, sectorParent.transform);

                    bgLeft.transform.position = new Vector3((sector - 0.25f) * sectorDimensions.x, 0, 10);
                    bgLeft.transform.localScale = new Vector3(sectorDimensions.x, sectorDimensions.y, 1);

                    bgRight.transform.position = new Vector3((sector + 0.25f) * sectorDimensions.x, 0, 10);
                    bgRight.transform.localScale = new Vector3(sectorDimensions.x, sectorDimensions.y, 1);

                }
                generatedSectors.Add(sector);
            }
        }
    }

    private Vector2 RandomSectorPosition(int sector)
    {
        return transform.position +
            sector * sectorDimensions.x * Vector3.right +
            (Random.value - .5f) * sectorDimensions.x * Vector3.right +
            (Random.value - .5f) * sectorDimensions.y * Vector3.up;
    }

    private void CleanupFarSectors(int sector)
    {
        List<int> obsoleteSectors = new List<int>();

        foreach (var sec in generatedSectorParents.Keys)
            if (Mathf.Abs(sec - sector) > deleteSectorsAtDistance)
                obsoleteSectors.Add(sec);

        foreach (var sec in obsoleteSectors)
        {
            Destroy(generatedSectorParents[sec]);
            generatedSectorParents.Remove(sec);
            generatedSectors.Remove(sec);
        }
    }

    private void OnDrawGizmos()
    {
        Color gizmoColor = Color.red;
        gizmoColor.a = .3f;

        Gizmos.color = gizmoColor;
        Gizmos.DrawCube(transform.position, sectorDimensions);

        gizmoColor = Color.yellow;
        gizmoColor.a = .1f;

        Gizmos.color = gizmoColor;
        Gizmos.DrawCube(transform.position + sectorDimensions.x * Vector3.left, sectorDimensions);
        Gizmos.DrawCube(transform.position + sectorDimensions.x * Vector3.right, sectorDimensions);
    }
}
