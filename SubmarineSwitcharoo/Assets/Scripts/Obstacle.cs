﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacle : MonoBehaviour {

    float minDistanceToPlayerSpawn = 15f;

    private void Start()
    {
        if ((PlayerControl.instance.transform.position - transform.position).magnitude < minDistanceToPlayerSpawn)
            Destroy(gameObject);
    }

    protected void OnCollisionEnter2D(Collision2D collision)
    {
        //If obstacle collide destroy 1 
        if (collision.gameObject.layer == LayerMask.NameToLayer("Obstacle"))
            Destroy(gameObject);
    }
}
