﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuAfterAnyKey : MonoBehaviour {

    public string mainMenuName = "StartScreen";

    private bool canChange = false;

	void Start () {
        StartCoroutine(ProhibitPress());
	}
	
	void Update () {
        if (canChange && Input.anyKeyDown)
            UnityEngine.SceneManagement.SceneManager.LoadScene(mainMenuName);
	}

    private IEnumerator ProhibitPress()
    {
        yield return new WaitForSeconds(5f);
        canChange = true;
    }
}
