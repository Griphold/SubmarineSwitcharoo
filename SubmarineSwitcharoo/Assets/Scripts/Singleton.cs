﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Singleton<T> : MonoBehaviour where T : class {

    public static T instance { get; set; }

    [SerializeField]
    protected bool dontDestroyOnLoad = false;

	void Awake () {
		if (null == instance)
        {
            instance = this as T;

            if (dontDestroyOnLoad)
                DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
	}
}
