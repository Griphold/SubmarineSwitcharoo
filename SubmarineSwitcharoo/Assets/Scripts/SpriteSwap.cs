﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteSwap : MonoBehaviour {

    public GameObject a, b;

    public void ToSpriteA()
    {
        a.SetActive(true);
        b.SetActive(false);
    }

    public void ToSpriteB()
    {
        a.SetActive(false);
        b.SetActive(true);
    }
}
