﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class SubmarinePart : MonoBehaviour
{

    public enum Type
    {
        GUN, HOOK, PROPELLER
    }

    public Type PartType;
    public float TurnSpeed;
    public Rigidbody2D SubmarineRigidbody;

    protected PlayerControl.Player ControllingPlayer;
    protected bool IsTurnable = true;

    public void RotatePart(float angle)
    {
        if (IsTurnable)
        {
            Vector3 eulerAngles = transform.eulerAngles;
            transform.eulerAngles = new Vector3(eulerAngles.x, eulerAngles.y, Mathf.LerpAngle(eulerAngles.z, angle, Time.deltaTime * TurnSpeed));
        }
    }

    public void RotatePart(float angle, bool lerp)
    {
        if (lerp)
        {
            RotatePart(angle);
        }
        else
        {
            Vector3 eulerAngles = transform.eulerAngles;
            transform.eulerAngles = new Vector3(eulerAngles.x, eulerAngles.y, angle);
        }
    }

    public void SetControllingPlayer(PlayerControl.Player player)
    {
        ControllingPlayer = player;

        //set color of object
        //transform.GetChild(0).GetComponent<Renderer>().material.color = SessionInfo.instance.playerColors[(int)player];
        ColorSprites(PlayerControl.instance.playerColors[(int)player]);
    }
    
    public PlayerControl.Player GetControllingPlayer()
    {
        return ControllingPlayer;
    }

    public abstract void Activate(bool active);

    public abstract void ColorSprites(Color color);
}
