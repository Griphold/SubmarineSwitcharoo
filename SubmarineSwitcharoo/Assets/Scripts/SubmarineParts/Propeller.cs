﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Propeller : SubmarinePart {
    public float Acceleration;
    public Sprite IdleSprite;
    public List<Sprite> TurningSprites = new List<Sprite>();
    public SpriteRenderer spriteRenderer;

    public ParticleSystem BubbleParticleSystem;
    public AudioSource PropellerSound;

    private int currentTurningSprite;
    private float startEmissionRate;

    public void Start()
    {
        spriteRenderer.sprite = IdleSprite;

        currentTurningSprite = TurningSprites.Count - 1;

        startEmissionRate = BubbleParticleSystem.emissionRate;

        BubbleParticleSystem.emissionRate = 0;
    }

    public override void Activate(bool active)
    {
        if (active)
        {
            //add acceleration from propeller in opposite direction of joystick axis
            SubmarineRigidbody.AddForce(-transform.up * Acceleration * SubmarineRigidbody.mass);

            //animate propeller
            currentTurningSprite = (currentTurningSprite + 1) % TurningSprites.Count;
            spriteRenderer.sprite = TurningSprites[currentTurningSprite];

            //set emit rate of particle system
            BubbleParticleSystem.emissionRate = startEmissionRate;

            if (!PropellerSound.isPlaying)
                PropellerSound.Play();
        }
        else
        {
            //set idle sprite
            spriteRenderer.sprite = IdleSprite;

            //set emit rate of particle system
            BubbleParticleSystem.emissionRate = 0;

            if (PropellerSound.isPlaying)
            {
                PropellerSound.Stop();
            }
        }
    }

    public override void ColorSprites(Color color)
    {
        spriteRenderer.color = color;
    }
}
