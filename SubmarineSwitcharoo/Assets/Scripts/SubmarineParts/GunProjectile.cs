﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider2D))]

public class GunProjectile : MonoBehaviour {
    public float Speed;
    public PlayerControl.Player player;
    public float Lifetime;

    private float Spawntime;
    private Vector3 direction;
    // Use this for initialization
    void Start () {
        Spawntime = Time.time;
        Speed += PlayerControl.instance.GetComponent<Rigidbody2D>().velocity.magnitude;
        direction = transform.up;
	}
	
	// Update is called once per frame
	void Update () {
        transform.Translate(direction * Speed * Time.deltaTime, Space.World);
        transform.Rotate(0, 0, 200 * Time.deltaTime);

        if(Spawntime + Lifetime < Time.time)
        {
            Destroy(gameObject);
        }
	}

    public void OnCollisionEnter2D(Collision2D collision)
    {
        Destroy(gameObject);
    }
}
