﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hook : SubmarinePart
{
    public float ExtendSpeed;
    public float RetractSpeed;
    public float MaxExtension;
    public AnimationCurve ExtensionCurve;
    public float SpringFrequency;
    public float SpringUnhookDistance;

    public GameObject ClawObject;

    public AudioSource GrapplingFX;

    //for open and close animation
    public Transform LeftClawPart;
    public Transform RightClawPart;
    public float ClawOpenAngle;
    private Vector3 leftClawEulerAngles;
    private Vector3 rightClawEulerAngles;

    private Vector3 clawStartingPosition;
    private Vector3 clawStartingAngles;
    private float currentExtension;
    private bool buttonHeld;

    private bool ClawGrappled;
    private SpringJoint2D clawJoint;

    private List<SpriteRenderer> objectsToColor = new List<SpriteRenderer>();

    //list of collectibles in claw
    private List<Collectible> collectiblesInClaw = new List<Collectible>();

    public void Awake()
    {
        objectsToColor.Add(transform.GetChild(0).GetComponent<SpriteRenderer>());
        objectsToColor.Add(LeftClawPart.GetComponent<SpriteRenderer>());
        objectsToColor.Add(RightClawPart.GetComponent<SpriteRenderer>());
    }

    public void Start()
    {
        clawStartingPosition = ClawObject.transform.localPosition;
        clawStartingAngles = ClawObject.transform.localEulerAngles;

        leftClawEulerAngles = LeftClawPart.transform.localEulerAngles;
        rightClawEulerAngles = RightClawPart.transform.localEulerAngles;
    }

    public override void Activate(bool active)
    {
        buttonHeld = active;
    }

    // Update is called once per frame
    void Update()
    {
        //extend when held
        if (!ClawGrappled)
        {
            if (buttonHeld && !colliding)
            {
                currentExtension += ExtendSpeed * Time.deltaTime;

            }
            //retract when not held
            else
            {
                currentExtension -= RetractSpeed * Time.deltaTime;
            }
            //clamp extension
            if(currentExtension < 0)
            {
                currentExtension = 0;
                OnCollect();
            }
            else if(currentExtension > MaxExtension)
            {
                currentExtension = MaxExtension;
            }
            
            ClawObject.transform.localPosition = clawStartingPosition + Vector3.up * MaxExtension * ExtensionCurve.Evaluate(currentExtension/MaxExtension);
        }
        else
        {
            //set anchor position at hook base, changes every frame
            clawJoint.anchor = transform.localPosition;

            //check distance of hook
            Vector2 clawPosition = ClawObject.transform.TransformPoint(clawJoint.connectedAnchor);
            Vector2 hookBasePosition = clawJoint.transform.position;

            if (Vector2.Distance(clawPosition, hookBasePosition) < SpringUnhookDistance)
            {
                //remove joint when we are close enough
                Destroy(clawJoint);
                Rigidbody2D clawRigidbody = ClawObject.GetComponent<Rigidbody2D>();
                //clawRigidbody.isKinematic = false;
                clawRigidbody.constraints = RigidbodyConstraints2D.FreezeRotation;

                ClawObject.transform.parent = transform;
                ClawObject.transform.localPosition = clawStartingPosition;
                ClawObject.transform.localEulerAngles = clawStartingAngles;

                //reset flag
                ClawGrappled = false;
                currentExtension = 0;
                clawJoint.connectedBody.isKinematic = true;
            }
        }

        //hook is only turnable when not extended
        IsTurnable = !ClawGrappled;

        //open or close claw
        Vector3 leftEulerTarget, rightEulerTarget;
        if (!isOpen())
        {
            leftEulerTarget = leftClawEulerAngles + new Vector3(0, 0, ClawOpenAngle);
            rightEulerTarget = rightClawEulerAngles + new Vector3(0, 0, -ClawOpenAngle);
        }
        else
        {
            leftEulerTarget = leftClawEulerAngles;
            rightEulerTarget = rightClawEulerAngles;
        }

        LeftClawPart.localEulerAngles = Vector3.Lerp(LeftClawPart.localEulerAngles, leftEulerTarget, Time.deltaTime * 20);
        RightClawPart.localEulerAngles = Vector3.Lerp(RightClawPart.localEulerAngles, rightEulerTarget, Time.deltaTime * 20);
    }

    public void FixedUpdate()
    {
        colliding = false;
    }

    public void OnClawCollisionEnter(Collision2D collision)
    {
        if (isExtended())
        {
            if (!ClawGrappled)
            {
                //found an obstacle
                if (collision.gameObject.layer == LayerMask.NameToLayer("Obstacle"))
                {
                    //set flag
                    ClawGrappled = true;

                    Rigidbody2D clawRigidbody = ClawObject.GetComponent<Rigidbody2D>();
                    //clawRigidbody.isKinematic = true;
                    clawRigidbody.constraints = RigidbodyConstraints2D.FreezePosition | RigidbodyConstraints2D.FreezeRotation;

                    ClawObject.transform.SetParent(null, true);

                    SpringJoint2D springJoint = PlayerControl.instance.gameObject.AddComponent<SpringJoint2D>();
                    springJoint.autoConfigureConnectedAnchor = false;
                    springJoint.autoConfigureDistance = false;

                    //springjoint anchor at hook base
                    springJoint.anchor = transform.localPosition;

                    springJoint.connectedBody = clawRigidbody;
                    springJoint.frequency = SpringFrequency;
                    springJoint.distance = 0.1f;

                    clawJoint = springJoint;

                    //play sound
                    if(GrapplingFX)
                        GrapplingFX.Play();
                }
            }
        }
    }

    private bool colliding = false;
    public void OnClawCollisionStay(Collision2D collision)
    {
        colliding = true;
    }

    public void OnCollectibleGrab(Collectible c)
    {
        if(isExtended() && !ClawGrappled)
        {
            //turn off collider and rigidbody of collectible
            c.gameObject.GetComponent<Collider2D>().enabled = false;
            c.gameObject.GetComponent<Rigidbody2D>().simulated = false;

            //child collectible to clawobject
            c.transform.SetParent(ClawObject.transform, true);
            c.transform.localPosition = Vector3.zero;

            //add collectible to collectible list
            collectiblesInClaw.Add(c);



            //SessionInfo.instance.AddPoints(c.GetAffiliation()[0], c.pointValue);
            /*Destroy(c.gameObject);*/
        }
    }

    private void OnCollect()
    {
        int[] cumulativePointsPerPlayer = new int[3] { 0,0,0};
        foreach(Collectible c in collectiblesInClaw)
        {
            SessionInfo.instance.AddPoints(c.GetAffiliation()[0], c.pointValue, transform.position);
            PlayerControl.Player p = c.GetAffiliation()[0];
            if (p == PlayerControl.Player.ONE)
            {
                cumulativePointsPerPlayer[0] += c.pointValue;
            }
            else if (p == PlayerControl.Player.TWO)
            {
                cumulativePointsPerPlayer[1] += c.pointValue;
            }
            else
            {
                cumulativePointsPerPlayer[2] += c.pointValue;
            }

            Destroy(c.gameObject);
        }

        if(cumulativePointsPerPlayer[0] > 0)
        {
            SessionInfo.instance.AddPoints(PlayerControl.Player.ONE, cumulativePointsPerPlayer[0], transform.position);
        }

        if(cumulativePointsPerPlayer[1] > 0)
        {
            SessionInfo.instance.AddPoints(PlayerControl.Player.TWO, cumulativePointsPerPlayer[1], transform.position);
        }

        if(cumulativePointsPerPlayer[2] > 0)
        {
            SessionInfo.instance.AddPoints(PlayerControl.Player.THREE, cumulativePointsPerPlayer[2], transform.position);
        }

        collectiblesInClaw.Clear();
    }

    private bool isExtended()
    {
        return currentExtension > 0;
    }

    private bool isOpen()
    {
        return (!buttonHeld && isExtended()) || ClawGrappled;
    }

    public override void ColorSprites(Color color)
    {
        foreach(SpriteRenderer sr in objectsToColor)
        {
            //Debug.Log(sr);
            sr.color = color;
        }
    }
}
