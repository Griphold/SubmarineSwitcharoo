﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun : SubmarinePart {
    //amount of shots per second
    public float FireRate;
    public GameObject Projectile;
    public float ProjectileOffset;
    public float RecoilForce;
    public ParticleSystem CannonSmoke;
    public AudioSource[] ProjectileSound;

    private float timeOfLastFire = 0f;

    private SpriteRenderer gunBase;

    public void Awake()
    {
        gunBase = transform.GetChild(0).GetComponent<SpriteRenderer>();
    }

    public override void Activate(bool active)
    {
        if (active)
        {
            if (Time.time - timeOfLastFire > 1/FireRate)
            {
                //fire projectile if holding button and within rate of fire
                Fire();
                timeOfLastFire = Time.time;
            }
        }
    }

    private int SoundIndex = 1;
    private void Fire()
    {
        //create projectile
        GameObject g = Instantiate(Projectile, transform.position + transform.up * ProjectileOffset, transform.rotation);
        GunProjectile p = g.GetComponent<GunProjectile>();
        p.player = ControllingPlayer;

        //Add smoke
        CannonSmoke.Play();

        //play sound
        //SoundIndex = (SoundIndex + 1) % 2;
        ProjectileSound[Random.Range(0, 1)].Play();

        //add recoil
        StartCoroutine(ApplyRecoil());
        //SubmarineRigidbody.AddForce(-transform.up * RecoilForce * SubmarineRigidbody.mass, ForceMode2D.Impulse);
    }

    private IEnumerator ApplyRecoil()
    {

        Vector2 recoilDirection = new Vector2(transform.up.x, transform.up.y);

        SubmarineRigidbody.velocity += -recoilDirection * RecoilForce * Time.fixedDeltaTime;
        yield return new WaitForSeconds(0.15f);

        for(int i = 0; i < 10; i++)
        {
            SubmarineRigidbody.velocity += recoilDirection * RecoilForce * 0.075f * Time.fixedDeltaTime;
            yield return new WaitForFixedUpdate();
        }
    }

    public override void ColorSprites(Color color)
    {
        gunBase.color = color;
    }
}
