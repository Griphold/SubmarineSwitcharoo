﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HookClaw : MonoBehaviour {
    public Hook parent;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnCollisionEnter2D(Collision2D collision)
    {
        parent.OnClawCollisionEnter(collision);
    }

    void OnCollisionStay2D(Collision2D collision)
    {
        parent.OnClawCollisionStay(collision);
    }
}
