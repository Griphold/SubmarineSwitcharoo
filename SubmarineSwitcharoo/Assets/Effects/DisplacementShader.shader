﻿Shader "ImageEffects/Displacement" 
{
	Properties 
	{
		_MainTex ("Base (RGB)", 2D) = "white" {}
		_Strength ("Displacement Strength", Range(0, 0.01)) = 0
		_ScrollSpeed ("Scroll Speed", Range(0, 10)) = 0
		_YTiling("Y Tiling", Range(0, 10)) = 0
	}
	SubShader {
		Pass {
			CGPROGRAM
			#pragma vertex vert_img
			#pragma fragment frag
 
			#include "UnityCG.cginc"
 
			uniform sampler2D _MainTex;
			uniform float _Strength;
			uniform float _ScrollSpeed;
			uniform float _YTiling;


			fixed4 frag(v2f_img i) : COLOR 
			{
				//scroll vertically
				float2 displUV = i.uv;
				displUV.y += _Time.y * _ScrollSpeed;

				//displace on Sine
				i.uv.x += sin(displUV.y * 3.1415f * 2.0f * _YTiling) * _Strength;
				i.uv = saturate(i.uv);
 
				fixed4 c = tex2D(_MainTex, i.uv);
				return c;
			}

			ENDCG
		}
	}
}
