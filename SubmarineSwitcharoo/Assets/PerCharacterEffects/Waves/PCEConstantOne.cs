﻿using UnityEngine;
using System.Collections;
using System;

namespace PerCharacterEffects
{
    public class PCEConstantOne : PCEWave
    {
        public override float Evaluate(float t, bool positiveOnly, CharacterTransformInformation characterTransform = null)
        {
            return 1f;
        }
    }
}