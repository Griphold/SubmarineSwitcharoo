﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

namespace PerCharacterEffects
{
    public class PCESine : PCEWave
    {
        private float t = 0f;

        public float frequency = 1f;

        public override float Evaluate(float t, bool positiveOnly, CharacterTransformInformation characterTransform = null)
        {
            if (positiveOnly)
                return (Mathf.Sin(this.t * Mathf.PI * 3f + t * Mathf.PI * 2f) + 1f) / 2f;
            else
                return Mathf.Sin((this.t + t) * Mathf.PI * 2f);
        }

        protected override IEnumerator WaveCoroutine()
        {
            t = 0f;

            while (true)
            {
                t += Time.deltaTime * frequency;
                yield return null;
            }
        }
    }
}