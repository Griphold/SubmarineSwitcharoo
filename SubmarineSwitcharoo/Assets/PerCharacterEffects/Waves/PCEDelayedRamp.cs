﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

namespace PerCharacterEffects
{
    public class PCEDelayedRamp : PCEWave
    {
        private float t = 0f;
        public float speed = 1f;

        [SerializeField]
        private float _rampWidth = 1f;
        public float rampWidth
        {
            get
            {
                return _rampWidth;
            }
            set
            {
                _rampWidth = Mathf.Max(0.01f, value);
            }
        }

        public enum Mode { LowToHigh, HighToLow }
        public Mode direction = Mode.LowToHigh;

        public override float Evaluate(float t, bool positiveOnly, CharacterTransformInformation characterTransform = null)
        {
            float ret = Mathf.SmoothStep(positiveOnly ? 0f : -1f, 1f, Mathf.Max(0f, this.t - t) / rampWidth);

            if (Mode.LowToHigh == direction)
                return ret;
            else
                return positiveOnly ? (1 - ret) : (-1f * ret);
        }

        protected override IEnumerator WaveCoroutine()
        {
            t = 0f;

            while (true)
            {
                t += Time.deltaTime * speed;
                yield return null;
            }
        }

#if UNITY_EDITOR
        private void OnValidate()
        {
            rampWidth = _rampWidth;
        }
#endif
    }
}