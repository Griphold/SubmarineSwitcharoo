﻿using UnityEngine;
using System.Collections;
using System;

namespace PerCharacterEffects
{
    public class PCESwipe : PCEWave
    {
        private float t = 0f;

        private bool onReturnRun = false;

        [SerializeField]
        private float _swipeRadius = 0.5f;
        public float swipeRadius
        {
            get
            {
                return _swipeRadius;
            }
            set
            {
                _swipeRadius = Mathf.Max(0f, value);
            }
        }

        public bool looping = false;

        public float loopCooldown = 1f;
        public float pingPongCooldown = 1f;

        public bool pingPong = false;

        public bool stopAtPeakLeft = false, stopAtPeakRight = false;

        public bool startRight = false;
        public bool inverseValue = false;

        public override float Evaluate(float t, bool positiveOnly, CharacterTransformInformation characterTransform = null)
        {
            float seekerPos = onReturnRun ? 1 - characterTransform.originalHorizontalPositionNormalized : characterTransform.originalHorizontalPositionNormalized;

            float distanceToPeak = Mathf.Min(1f, Mathf.Abs(this.t - seekerPos) / swipeRadius);

            if (inverseValue)
                return -Mathf.SmoothStep(positiveOnly ? 0f : 1f, -1f, distanceToPeak);
            else
                return Mathf.SmoothStep(1f, positiveOnly ? 0f : -1f, distanceToPeak);
            
        }

        protected override IEnumerator WaveCoroutine()
        {
            onReturnRun = startRight;

            t = (stopAtPeakRight ? 1f : 1f + swipeRadius);

            if (pingPong)
                onReturnRun = !onReturnRun;

            do
            {
                if (pingPong)
                    onReturnRun = !onReturnRun;

                t = (stopAtPeakRight ? 1f : 1f + swipeRadius) - t;
                t += stopAtPeakLeft ? 0f : -swipeRadius;

                while (t < (stopAtPeakRight ? 1f : 1f + swipeRadius))
                {
                    t += Time.deltaTime;
                    yield return null;
                }

                t = (stopAtPeakRight ? 1f : 1f + swipeRadius) - t;
                t += stopAtPeakLeft ? 0f : -swipeRadius;

                if (pingPong)
                {
                    onReturnRun = !onReturnRun;
                    yield return new WaitForSeconds(pingPongCooldown);
                }
                else if (looping)
                    yield return new WaitForSeconds(loopCooldown);
                else
                    yield break;

                while (t < (stopAtPeakRight ? 1f : 1f + swipeRadius))
                {
                    t += Time.deltaTime;
                    yield return null;
                }

                yield return new WaitForSeconds(loopCooldown);
            } while (looping);
        }

#if UNITY_EDITOR
        private void OnValidate()
        {
            swipeRadius = _swipeRadius;
        }
#endif
    }
}
