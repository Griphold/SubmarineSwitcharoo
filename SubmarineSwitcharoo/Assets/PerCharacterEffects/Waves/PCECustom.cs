﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

namespace PerCharacterEffects
{
    public class PCECustom : PCEWave
    {
        public AnimationCurve curve;

        public override float Evaluate(float t, bool positiveOnly, CharacterTransformInformation characterTransform = null)
        {
            if (positiveOnly)
                return Mathf.Max(0f, curve.Evaluate(characterTransform.originalHorizontalPositionNormalized));
            else
                return curve.Evaluate(characterTransform.originalHorizontalPositionNormalized);
        }
    }
}