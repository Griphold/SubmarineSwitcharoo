﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace PerCharacterEffects
{
    public class PCEScale : PCECharacterEffector
    {
        public Vector3 additiveScaling = Vector3.zero;
        public bool additive = false;

        private Dictionary<CharacterTransformInformation, Vector3> additiveScales = new Dictionary<CharacterTransformInformation, Vector3>();

        protected override void CharacterUpdate(CharacterTransformInformation characterTransform, float strengthRaw, float strengthPositive)
        {
            if (additive)
            {
                Vector3 additiveScale = additiveScales[characterTransform];

                additiveScale += additiveScaling * Time.deltaTime * strengthPositive;

                additiveScales[characterTransform] = additiveScale;

                characterTransform.scaling = Vector3.one + additiveScale;
            }
            else
            {
                characterTransform.scaling = Vector3.one + strengthPositive * additiveScaling;
            }
        }

        protected override void MeshChangedHandler(ReadOnlyCollection<CharacterTransformInformation> characterTransforms)
        {
            additiveScales.Clear();

            foreach (var cti in characterTransforms)
            {
                additiveScales.Add(cti, Vector3.zero);
            }
        }
    }
}