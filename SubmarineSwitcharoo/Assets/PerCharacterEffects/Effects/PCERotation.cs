﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace PerCharacterEffects
{
    public class PCERotation : PCECharacterEffector
    {
        public Vector3 angles = Vector3.zero;
        public bool additive = false;
        public bool positiveOnly = false;

        private Dictionary<CharacterTransformInformation, Vector3> additiveAngles = new Dictionary<CharacterTransformInformation, Vector3>();

        protected override void CharacterUpdate(CharacterTransformInformation characterTransform, float strengthRaw, float strengthPositive)
        {
            if (additive)
            {
                Vector3 additiveAngle = additiveAngles[characterTransform];

                additiveAngle += angles * Time.deltaTime * strengthPositive;

                // Avoid numerical errors
                additiveAngle.x %= 360f;
                additiveAngle.y %= 360f;
                additiveAngle.z %= 360f;

                additiveAngles[characterTransform] = additiveAngle;

                characterTransform.rotation = additiveAngle;
            }
            else
            {
                characterTransform.rotation += (positiveOnly ? strengthPositive : strengthRaw) * angles;
            }
        }

        protected override void MeshChangedHandler(ReadOnlyCollection<CharacterTransformInformation> characterTransforms)
        {
            additiveAngles.Clear();

            foreach (var cti in characterTransforms)
            {
                additiveAngles.Add(cti, Vector3.zero);
            }
        }
    }
}