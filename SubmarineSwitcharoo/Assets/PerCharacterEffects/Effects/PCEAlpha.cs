﻿using UnityEngine;
using System.Collections;
using System;

namespace PerCharacterEffects
{
    public class PCEAlpha : PCECharacterEffector
    {
        public float alpha;

        protected override void CharacterUpdate(CharacterTransformInformation characterTransform, float strengthRaw, float strengthPositive)
        {
            characterTransform.alphaOnly = Mathf.Lerp(characterTransform.originalColor.a, alpha, strengthPositive);
        }
    }
}