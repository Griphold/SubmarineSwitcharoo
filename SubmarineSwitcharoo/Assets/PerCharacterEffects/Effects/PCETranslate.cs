﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace PerCharacterEffects
{
    public class PCETranslate : PCECharacterEffector
    {
        public Vector3 translation = Vector3.zero;
        public bool additive = false;

        private Dictionary<CharacterTransformInformation, Vector3> additiveTranslation = new Dictionary<CharacterTransformInformation, Vector3>();

        public float leftCharStrengthMultiplier = 1f, rightCharStrengthMultiplier = 1f;

        protected override void CharacterUpdate(CharacterTransformInformation characterTransform, float strengthRaw, float strengthPositive)
        {
            if (additive)
            {
                Vector3 additiveAngle = additiveTranslation[characterTransform];

                strengthPositive *= Mathf.Lerp(leftCharStrengthMultiplier, rightCharStrengthMultiplier, characterTransform.originalHorizontalPositionNormalized);

                additiveAngle += translation * Time.deltaTime * strengthPositive;

                additiveTranslation[characterTransform] = additiveAngle;

                characterTransform.translation += additiveAngle;
            }
            else
            {
                characterTransform.translation += strengthRaw * translation;
            }
        }

        protected override void MeshChangedHandler(ReadOnlyCollection<CharacterTransformInformation> characterTransforms)
        {
            additiveTranslation.Clear();

            foreach (var cti in characterTransforms)
            {
                additiveTranslation.Add(cti, Vector3.zero);
            }
        }
    }
}