﻿using UnityEngine;
using System.Collections;
using System;

namespace PerCharacterEffects
{
    public class PCEColor : PCECharacterEffector
    {
        public Color color;

        protected override void CharacterUpdate(CharacterTransformInformation characterTransform, float strengthRaw, float strengthPositive)
        {
            characterTransform.colorOnly = Color.Lerp(characterTransform.originalColor, color, strengthPositive);
        }
    }
}