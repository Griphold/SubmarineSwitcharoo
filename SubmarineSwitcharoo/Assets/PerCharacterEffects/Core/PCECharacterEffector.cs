﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace PerCharacterEffects
{
    [RequireComponent(typeof(PerCharacterEffect))]
    public abstract class PCECharacterEffector : MonoBehaviour
    {
        private PerCharacterEffect pce;

        private int lastTextLength = int.MaxValue;
        private bool textIsLonger = false;

        [SerializeField]
        private PCEWave wave;

        [SerializeField]
        private float firstCharOffset = 0f;

        [SerializeField]
        private float perCharacterOffset = 0f;

        public enum TargetCharacters { AllCharacters, NewestCharacter }
        public TargetCharacters affects = TargetCharacters.AllCharacters;

        [SerializeField]
        [Range(-1f, 1f)]
        private float _rawStrength = 1f;
        public float rawStrength
        {
            get
            {
                return _rawStrength;
            }

            set {
                _rawStrength = Mathf.Clamp(value, -1f, 1f);
            }
        }

        private void Awake()
        {
            pce = GetComponent<PerCharacterEffect>();

            pce.OnMeshUpdate += MeshUpdateHandler;
            pce.OnMeshChanged += MeshChangedHandlerInternal;
        }

        private void OnDestroy()
        {
            pce.OnMeshUpdate -= MeshUpdateHandler;
            pce.OnMeshChanged -= MeshChangedHandlerInternal;
        }

        private void MeshUpdateHandler(ReadOnlyCollection<CharacterTransformInformation> characterTransforms)
        {
            if (null == wave)
                throw new System.Exception("You need to assign a per character effect wave");

            if (TargetCharacters.AllCharacters == affects)
            {
                float offset = firstCharOffset;

                foreach (var cti in characterTransforms)
                {
                    CharacterUpdate(cti, rawStrength * wave.Evaluate(offset, false, cti), rawStrength * wave.Evaluate(offset, true, cti));

                    offset += perCharacterOffset;
                }
            }
            else if (textIsLonger)
            {
                CharacterTransformInformation cti = characterTransforms[characterTransforms.Count - 1];

                CharacterUpdate(cti, rawStrength * wave.Evaluate(firstCharOffset, false, cti), rawStrength * wave.Evaluate(firstCharOffset, true, cti));
            }
        }

        private void MeshChangedHandlerInternal(ReadOnlyCollection<CharacterTransformInformation> characterTransforms)
        {
            textIsLonger = lastTextLength < characterTransforms.Count;

            lastTextLength = characterTransforms.Count;

            MeshChangedHandler(characterTransforms);
        }

        protected abstract void CharacterUpdate(CharacterTransformInformation characterTransform, float strengthRaw, float strengthPositive);

        protected virtual void MeshChangedHandler(ReadOnlyCollection<CharacterTransformInformation> characterTransforms) { }
    }
}