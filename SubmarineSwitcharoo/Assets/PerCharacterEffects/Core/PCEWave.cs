﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace PerCharacterEffects
{
    [RequireComponent(typeof(PerCharacterEffect))]
    public abstract class PCEWave : MonoBehaviour
    {
        private PerCharacterEffect pce;

        private Coroutine coroutine;

        private void Awake()
        {
            pce = GetComponent<PerCharacterEffect>();

            pce.OnMeshChanged += MeshChangedHandler;
        }

        public void OnDestroy()
        {
            pce.OnMeshChanged -= MeshChangedHandler;
        }

        private void MeshChangedHandler(ReadOnlyCollection<CharacterTransformInformation> characterTransforms)
        {
            if (null != coroutine)
                StopCoroutine(coroutine);

            coroutine = StartCoroutine(WaveCoroutine());
        }

        protected virtual IEnumerator WaveCoroutine()
        {
            yield return null;
        }

        public abstract float Evaluate(float t, bool positiveOnly, CharacterTransformInformation characterTransform = null);
    }
}