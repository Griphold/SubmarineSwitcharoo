﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System;
using System.Linq;

namespace PerCharacterEffects
{
    public interface CharacterTransformInformation
    {
        char character { get; }
        int index { get; }
        Vector3 originalCenter { get; }
        Color originalColor { get; }
        float originalHorizontalPositionNormalized { get; }

        Vector3 translation { get; set; }
        Vector3 rotation { get; set; }
        Vector3 scaling { get; set; }
        Color colorAndAlpha { get; set; }
        float alphaOnly { get; set; }
        Color colorOnly { get; set; }

        void Reset();
    }

    [RequireComponent(typeof(Text))]
    public class PerCharacterEffect : MonoBehaviour, IMeshModifier
    {
        public delegate void MeshUpdateHandler(ReadOnlyCollection<CharacterTransformInformation> characterTransforms);
        public event MeshUpdateHandler OnMeshUpdate;

        public delegate void MeshChangedHandler(ReadOnlyCollection<CharacterTransformInformation> characterTransforms);
        public event MeshChangedHandler OnMeshChanged;

        private const int VERTS_PER_CHARACTER = 6;

        private CanvasRenderer canvasRenderer;

        private List<Character> characters;

        private List<UIVertex> uiVerts;

        private Mesh mesh;
        private List<Vector3> vertexPositions;
        private List<Vector3> vertexNormals;
        private List<Vector2> vertexUVs;
        private List<Color32> vertexColors;
        private List<int> triangleIndices;

        // Infos about current character batch
        public float leftmostCharacterCenter { get; private set; }
        public float rightmostCharacterCenter { get; private set; }

        private class Character
        {
            private class CharacterTransformInformationInternal : CharacterTransformInformation
            {
                private PerCharacterEffect pce;

                public char character { get; private set; }
                public int index { get; private set; }
                public Vector3 originalCenter { get; private set; }
                public Color originalColor { get; private set; }
                public float originalHorizontalPositionNormalized
                {
                    get
                    {
                        return (originalCenter.x - pce.leftmostCharacterCenter) / (pce.rightmostCharacterCenter - pce.leftmostCharacterCenter);
                    }
                }

                public Vector3 translation { get; set; }
                public Vector3 rotation { get; set; }
                public Vector3 scaling { get; set; }
                public Color colorAndAlpha { get; set; }
                public float alphaOnly
                {
                    get
                    {
                        return colorAndAlpha.a;
                    }
                    set
                    {
                        Color c = colorAndAlpha;
                        c.a = value;
                        colorAndAlpha = c;
                    }
                }
                public Color colorOnly
                {
                    get
                    {
                        return colorAndAlpha;
                    }
                    set
                    {
                        Color c = value;
                        c.a = colorAndAlpha.a;
                        colorAndAlpha = c;
                    }
                }

                public CharacterTransformInformationInternal(char character, int index, Vector3 originalCenter, Color originalColor, PerCharacterEffect pce)
                {
                    this.pce = pce;

                    this.character = character;
                    this.originalCenter = originalCenter;
                    this.originalColor = originalColor;

                    Reset();
                }

                public void Reset() // Resets the Character to its original state
                {
                    translation = Vector3.zero;
                    rotation = Vector3.zero;
                    scaling = Vector3.one;
                    colorAndAlpha = originalColor;
                }
            }

            public bool visible { get { return characterTransformInfo.colorAndAlpha.a != 0f; } }

            private List<UIVertex> _originalVertices, transformedVertices;
            public ReadOnlyCollection<UIVertex> originalVertices { get { return _originalVertices.AsReadOnly(); } }

            public CharacterTransformInformation characterTransformInfo { get; private set; }

            public Character(List<UIVertex> uiVerts, PerCharacterEffect pce, int index)
            {
                _originalVertices = uiVerts;
                transformedVertices = new List<UIVertex>();

                Vector3 originalCenter = (_originalVertices[0].position + _originalVertices[1].position + _originalVertices[3].position + _originalVertices[4].position) / 4f;

                Color avgCol = Color.clear;
                for (int i = 0; i < VERTS_PER_CHARACTER; i++)
                {
                    avgCol.r += (float)_originalVertices[i].color.r / 255f;
                    avgCol.g += (float)_originalVertices[i].color.g / 255f;
                    avgCol.b += (float)_originalVertices[i].color.b / 255f;
                    avgCol.a += (float)_originalVertices[i].color.a / 255f;
                }
                avgCol /= (float)VERTS_PER_CHARACTER;

                characterTransformInfo = new CharacterTransformInformationInternal('x', index, originalCenter, avgCol, pce);
            }

            public List<UIVertex> GetTransformedVertices()
            {
                Matrix4x4 toCenter = Matrix4x4.TRS(-characterTransformInfo.originalCenter, Quaternion.identity, Vector3.one);
                Matrix4x4 transform = toCenter.inverse * Matrix4x4.TRS(characterTransformInfo.translation, Quaternion.Euler(characterTransformInfo.rotation), characterTransformInfo.scaling) * toCenter;

                transformedVertices.Clear();
                transformedVertices.AddRange(_originalVertices);

                for (int i = 0; i < VERTS_PER_CHARACTER; i++)
                {
                    UIVertex tempVert = transformedVertices[i];
                    tempVert.position = transform * new Vector4(tempVert.position.x, tempVert.position.y, tempVert.position.z, 1f);
                    tempVert.color = characterTransformInfo.colorAndAlpha;
                    transformedVertices[i] = tempVert;
                }

                return transformedVertices;
            }
        }

        void IMeshModifier.ModifyMesh(VertexHelper vh)
        {
            if (!Application.isPlaying)
                return;

            vh.GetUIVertexStream(uiVerts);

            characters.Clear();

            leftmostCharacterCenter = float.PositiveInfinity;
            rightmostCharacterCenter = float.NegativeInfinity;
            for (int i = 0; i < uiVerts.Count; i += VERTS_PER_CHARACTER)
            {
                Character newChar = new Character(uiVerts.GetRange(i, VERTS_PER_CHARACTER), this, i / VERTS_PER_CHARACTER);
                characters.Add(newChar);
                leftmostCharacterCenter = Mathf.Min(leftmostCharacterCenter, newChar.characterTransformInfo.originalCenter.x);
                rightmostCharacterCenter = Mathf.Max(rightmostCharacterCenter, newChar.characterTransformInfo.originalCenter.x);
            }

            if (null != OnMeshChanged)
                OnMeshChanged(characters.Select(c => { return c.characterTransformInfo; }).ToList().AsReadOnly());

            EvaluateEffects();
            FillAndSetMesh();
            ResetCharacters();

            // For the first frame, we need to write the effects into a uivertex triangle stream
            uiVerts.Clear();
            for (int i = 0; i < vertexPositions.Count; i++)
            {
                if (!characters[i / 6].visible)
                {
                    i += VERTS_PER_CHARACTER - 1;
                    continue;
                }

                UIVertex v = new UIVertex();
                v.position = vertexPositions[i];
                v.normal = vertexNormals[i];
                v.uv0 = vertexUVs[i];
                v.color = vertexColors[i];

                uiVerts.Add(v);
            }

            vh.AddUIVertexTriangleStream(uiVerts);
        }

        void IMeshModifier.ModifyMesh(Mesh mesh)
        {
            throw new NotImplementedException();
        }

        private void Awake()
        {
            canvasRenderer = GetComponent<CanvasRenderer>();

            characters = new List<Character>();

            uiVerts = new List<UIVertex>();

            mesh = new Mesh();
            vertexPositions = new List<Vector3>();
            vertexNormals = new List<Vector3>();
            vertexUVs = new List<Vector2>();
            vertexColors = new List<Color32>();
            triangleIndices = new List<int>();
        }

        private void LateUpdate()
        {
            EvaluateEffects();
            FillAndSetMesh();
            ResetCharacters();
        }

        private void FillAndSetMesh()
        {
            mesh.Clear();

            vertexPositions.Clear();
            vertexNormals.Clear();
            vertexUVs.Clear();
            vertexColors.Clear();
            triangleIndices.Clear();

            int triangleIndex = 0;
            foreach (var character in characters)
            {
                if (character.visible)
                {
                    foreach (var vert in character.GetTransformedVertices())
                    {
                        vertexPositions.Add(vert.position);
                        vertexNormals.Add(vert.normal);
                        vertexUVs.Add(vert.uv0);
                        vertexColors.Add(vert.color);
                        triangleIndices.Add(triangleIndex++);
                    }
                }
            }

            mesh.vertices = vertexPositions.ToArray();
            mesh.normals = vertexNormals.ToArray();
            mesh.uv = vertexUVs.ToArray();
            mesh.colors32 = vertexColors.ToArray();
            mesh.triangles = triangleIndices.ToArray();

            canvasRenderer.SetMesh(mesh);
        }

        private void ResetCharacters()
        {
            foreach (var c in characters)
                c.characterTransformInfo.Reset();
        }

        private void EvaluateEffects()
        {
            if (null != OnMeshUpdate)
                OnMeshUpdate((characters.Select(c => { return c.characterTransformInfo; }).ToList().AsReadOnly()));
        }
    }
}