﻿using UnityEngine;
using UnityEditor;
using System.Collections;

namespace PerCharacterEffects
{
    [CustomEditor(typeof(PerCharacterEffect))]
    public class PerCharacterEffectEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            
        }
    }
}